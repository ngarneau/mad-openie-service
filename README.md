# Relation extraction microservice

The project provides an interface for extracting relations from sentences.

The service one REST endpoint;

* one which gives relations extracted for given sentence

## Usage

Start services with sbt:

```
$ sbt
> ~re-start
```

With the service up, you can start sending HTTP requests:

```
$ curl -X POST -H 'Content-Type: application/json' http://localhost:9000/extract -d '{"sentence": "Donal Trump is the new president of the United States."}'
[{
  "subject": "Donal Trump",
  "relation": "be",
  "obj": "president",
  "confidence": 1.0
}, {
  "subject": "Donal Trump",
  "relation": "be new president of",
  "obj": "United States",
  "confidence": 1.0
}, {
  "subject": "president",
  "relation": "be",
  "obj": "new",
  "confidence": 1.0
}, {
  "subject": "Donal Trump",
  "relation": "be",
  "obj": "new president",
  "confidence": 1.0
}, {
  "subject": "Donal Trump",
  "relation": "be president of",
  "obj": "United States",
  "confidence": 1.0
}]
```

### Testing

Execute tests using `test` command:

```
$ sbt
> test
```

### Packagin

Execute package using `package` command:

```
$ sbt
> assembly
```
