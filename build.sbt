enablePlugins(JavaAppPackaging)

name := "relation-extraction-microservice"
organization := "ca.ulaval.ift"
version := "1.0"
scalaVersion := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += Resolver.jcenterRepo

libraryDependencies ++= {
  val akkaV       = "2.4.3"
  val scalaTestV  = "2.2.6"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-http-experimental" % akkaV,
    "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaV,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaV,
    "org.allenai.openie" %% "openie" % "4.2.6",
    "org.scalatest"     %% "scalatest" % scalaTestV % "test"
  )
}

javaOptions += "-Xmx4G"
javaOptions += "-XX:+UseConcMarkSweepGC"

Revolver.settings
