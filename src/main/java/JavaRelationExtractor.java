import java.util.List;

interface JavaRelationExtractor extends RelationExtractor{
    List<Relation> getRelations(String s);
}
