/**
 * Created by ngarneau on 24/11/16.
 */

import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;

import java.util.*;

public class StanfordOpenIEExtractor {

    private StanfordCoreNLP pipeline;

    public StanfordOpenIEExtractor(){
        Properties props = PropertiesUtils.asProperties(
                "annotators", "tokenize,ssplit,pos,lemma,depparse,natlog,openie",
                "openie.triple.all_nominals", "true",
                "openie.splitter.nomodel", "true"
        );
        this.pipeline = new StanfordCoreNLP(props);
    }


    public List<Relation> getRelations(String s) {
        Annotation doc = new Annotation(s);
        this.pipeline.annotate(doc);

        List<Relation> relations = new ArrayList<>();
        List<String> dumpArgs = new ArrayList<>();

        for (CoreMap sentence : doc.get(CoreAnnotations.SentencesAnnotation.class)) {
            Collection<RelationTriple> triples = sentence.get(NaturalLogicAnnotations.RelationTriplesAnnotation.class);
            for (RelationTriple triple : triples) {
            }
        }
        return relations;
    }

}
