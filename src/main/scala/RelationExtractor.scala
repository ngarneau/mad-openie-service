/**
  * Created by ngarneau on 02/02/17.
  */

import java.util

import scala.collection.JavaConversions._
import edu.knowitall.openie.{Argument, Instance, OpenIE}

trait RelationExtractor {
  def getRelations(sentence: String): java.util.List[Relation]
}

object WashingtonOpenIE extends RelationExtractor {
  val extractor = new OpenIE

  override def getRelations(sentence: String): util.List[Relation] = {
    val rawRelations: Seq[Instance] = extractor.extract(sentence)
    rawRelations.map {
      instance => Relation(
        instance.extraction.arg1.displayText,
        instance.extraction.rel.text,
        instance.extraction.arg2s.map((s: Argument) => s.displayText).toList,
        instance.extraction.context.map(_.displayText),
        instance.extraction.negated,
        instance.extraction.passive,
        instance.confidence
      )
    }.toList
  }
}
