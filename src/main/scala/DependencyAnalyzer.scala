/**
  * Created by ngarneau on 24/02/17.
  */

import edu.knowitall.tool.parse.graph.DependencyGraph
import edu.knowitall.tool.parse.{ClearParser, DependencyParser}

object DependencyAnalyzer {

  def main(args: Array[String]): Unit = {
    val text = "PCB concentrations in OU1 were found and ranging from below detection levels to 4,000 mg/kg while concentrations in the OU2 Hot Spot Area ranged from 4,000 mg/kg to more than 200,000 mg/kg."
    val clearParser = new ClearParser()
    val dgraph = clearParser.dependencyGraph(text)
    println(dgraph.dot())
  }

}
