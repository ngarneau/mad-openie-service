import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.{Config, ConfigFactory}
import spray.json.DefaultJsonProtocol

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContextExecutor


case class Relation(
                     subject: String,
                     relation: String,
                     arguments: List[String],
                     context: Option[String],
                     negated: Boolean,
                     passive: Boolean,
                     confidence: Double
                   )
case class Sentence(sentence: String)


trait Protocols extends DefaultJsonProtocol {
  implicit val relationFormat = jsonFormat7(Relation.apply)
  implicit val sentenceFormat = jsonFormat1(Sentence.apply)
}

trait Service extends Protocols {
  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  def config: Config
  val logger: LoggingAdapter

  lazy val extractor = ExtractorFactory.createExtractor("Washington")
  def extractSentence(sentence: String): List[Relation] = extractor.getRelations(sentence).toList

  val routes = {
    logRequestResult("akka-http-microservice") {
      pathPrefix("extract") {
        (post & entity(as[Sentence])) { sentenceRequest =>
          complete {
            extractSentence(sentenceRequest.sentence)
          }
        }
      }
    }
  }
}

object ExtractorFactory {
  def createExtractor(name: String): RelationExtractor = name match {
    case "Washington" => WashingtonOpenIE
    case _ => WashingtonOpenIE
  }
}

object OpenIEMicroservice extends App with Service {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorMaterializer()

  override val config = ConfigFactory.load()
  override val logger = Logging(system, getClass)

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
