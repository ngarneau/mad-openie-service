import akka.event.NoLogging
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest._

class ServiceSpec extends FlatSpec with Matchers with ScalatestRouteTest with Service {
  override def testConfigSource = "akka.loglevel = WARNING"
  override def config = testConfig
  override val logger = NoLogging

  val relations = List(
    Relation("Obama", "be", List("president"), None, false, false, 1.0),
    Relation("Obama", "be president of", List("United States"), None, false, false, 1.0)
  )
  val sentence = Sentence("Obama is the president of the United States")

  override def extractSentence(sentence: String) = sentence.length match {
    case 0 => List()
    case _ => relations
  }

  "Service" should "respond to single sentence query" in {
    Post(s"/extract", sentence) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[List[Relation]] shouldBe relations
    }
    Post(s"/extract", Sentence("")) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[List[Relation]] shouldBe List()
    }
  }
}
